<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
</p>

## Development
Consumed JSON Requests from below.
Assume data below provided

- Users: https://jsonplaceholder.typicode.com/users
- Todo: https://jsonplaceholder.typicode.com/todos

Developed using Laravel 8.12.

## Live Demo
**[Link](http://iserve.kre.com.my/public/)**

