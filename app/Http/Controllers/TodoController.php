<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class TodoController extends Controller
{
    /**
     * @var string
     */
    private $todoUrl;
    /**
     * @var string
     */
    private $usersUrl;

    public function __construct()
    {
        $this->todoUrl = 'https://jsonplaceholder.typicode.com/todos';
        $this->usersUrl = 'https://jsonplaceholder.typicode.com/users';
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        return view('todo.index');
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getTopTodos(Request $request)
    {
        /*
         * Create Json Objects from the url.
         * Get Completed Todos.
         * Group by Users
         * add number or completed todos and completed todos
         * sort by number of comlpeted todos
         */
        $todos = $this->getJsonObjectFromUrl($this->todoUrl);
        $users = $this->getJsonObjectFromUrl($this->usersUrl);

        $topTodos = $todos->where('completed', true)->groupBy('userId')->map(function ($todo, $userId) use ($users) {
            return [
                'userId' => $userId,
                'completed_todos' => $todo,
                'completed_todos_count' => $todo->count(),
                'user' => $users->where('id', $userId)->first()
            ];
        })->sortByDesc('completed_todos_count');

        if ($request->ajax()) {
            return Datatables::of($topTodos)
                ->addIndexColumn()
                ->make(true);
        }
    }

    /**
     * @param $jsonUrl
     * @return \Illuminate\Support\Collection
     */
    private function getJsonObjectFromUrl($jsonUrl)
    {
        /* accept url and decode
         * return as collection to use eloquent methods
         */

        return collect(json_decode(file_get_contents($jsonUrl)));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function viewAllTodos()
    {
        /*
         * get first todos and get the keys
         */

        $todosFilters = collect($this->getJsonObjectFromUrl($this->todoUrl)->first())->except('id')->keys();

        return view('todo.filter', compact('todosFilters'));

    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function getAllTodos(Request $request)
    {
        /*
         * get the filter and values from request
         * map users to the todos
         * filter by the keywords
         */

        $filter = $request->filter;
        $value = $request->value;
        $users = $this->getJsonObjectFromUrl($this->usersUrl);
        $todos = $this->getJsonObjectFromUrl($this->todoUrl)->each(function ($todo) use ($users) {
            $todo->user = $users->where('id', $todo->userId)->first();
        });
        if ($filter) {
            $todos = $todos->filter(function ($todo) use ($filter, $value) {
                return false !== stristr($todo->$filter, $value);
            });
        }
        if ($request->ajax()) {
            return Datatables::of($todos)
                ->addIndexColumn()
                ->make(true);
        }
    }
}
