<!DOCTYPE html>
<html>
<head>
    <title>All Todos</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>

<a href="{{route('todo.home')}}">Top Completed Todo Users</a>
||
<a href="{{route('todo.view')}}">All Todos</a>
<div class="container mt-5">
    <h2 class="mb-4">All Todos</h2>
    <form action="{{route('todo.view')}}">
        <div class="col-3">
            <select name="filter" class="form-control">
                @foreach($todosFilters as $filter)
                    <option value="{{$filter}}">{{$filter}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-3">
            <input type="text" class="form-control" name="value"/>
        </div>
        <button class="btn btn-success" type="submit"> Search</button>
    </form>
    <button class="btn btn-warning" onclick="document.location.href='{{route('todo.view')}}'"> Reset</button>
    <table class="table table-bordered yajra-datatable">
        <thead>
        <tr>
            <th>No</th>
            <th>User</th>
            <th>Title</th>
            <th>Completed</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function () {

        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('todo.all',['filter'=>request('filter'),'value'=>request('value')]) }}",
            columns: [
                {
                    data: 'DT_RowIndex',
                },
                {
                    data: 'user.name',
                },
                {
                    data: 'title',
                },
                {
                    data: 'completed',
                },
                // {
                //     data: 'action',
                //     name: 'action'
                // },
            ],
        });

    });
</script>
</html>
