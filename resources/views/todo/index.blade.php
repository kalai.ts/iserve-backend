<!DOCTYPE html>
<html>
<head>
    <title>Top Completed Todo Users</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
</head>
<body>

<a href="{{route('todo.home')}}">Top Completed Todo Users</a>
||
<a href="{{route('todo.view')}}">All Todos</a>
<div class="container mt-5">
    <h2 class="mb-4">Top Completed Todo Users</h2>
    <table class="table table-bordered yajra-datatable">
        <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Number of Completed Todos</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
    $(function () {

        var table = $('.yajra-datatable').DataTable({
            processing: true,
            serverSide: true,
            searching:false,
            ajax: "{{ route('todo.get') }}",
            columns: [
                {
                    data: 'DT_RowIndex',
                },
                {
                    data: 'user.name',
                },
                {
                    data: 'user.email',
                },
                {
                    data: 'completed_todos_count',
                }
            ],
        });

    });
</script>
</html>
